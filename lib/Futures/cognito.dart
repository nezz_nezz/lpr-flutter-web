import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:flutter/material.dart';
import 'package:lprboilerplateflutter/util/showDialog.dart';
import '../conf/secret.dart';

class CognitoAuth {
  Future<Object> signIn(context, email, password) async {
    final userPool = new CognitoUserPool(cognitoUserPoolId , cognitoClientId);
    final cognitoUser = new CognitoUser(email, userPool);
    final authDetails = new AuthenticationDetails(
      username: email,
      password: password
    );
    CognitoUserSession session;
    try {
      session = await cognitoUser.authenticateUser(authDetails);
      Navigator.pushNamed(context, '/socks');
    } on CognitoUserException catch (e) {
      print (e);
    } on CognitoUserNewPasswordRequiredException catch (e) {
      // handle New Password challenge
    } on CognitoUserMfaRequiredException catch (e) {
      // handle SMS_MFA challenge
    } on CognitoUserSelectMfaTypeException catch (e) {
      // handle SELECT_MFA_TYPE challenge
    } on CognitoUserMfaSetupException catch (e) {
      // handle MFA_SETUP challenge
    } on CognitoUserTotpRequiredException catch (e) {
      // handle SOFTWARE_TOKEN_MFA challenge
    } on CognitoUserCustomChallengeException catch (e) {
      // handle CUSTOM_CHALLENGE challenge
    } on CognitoUserConfirmationNecessaryException catch (e) {
      // handle User Confirmation Necessary
    } catch (e) {
      openDialog(context, 'SignIn Error:', e.message);
      print(e);
    }
    return session;
  }
}