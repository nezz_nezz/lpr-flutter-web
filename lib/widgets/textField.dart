import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField({Key key, this.hintText}) : super(key: key);

  final String hintText;

  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Theme.of(context).primaryColor),
        borderRadius: new BorderRadius.all(Radius.circular(5))
      ),
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(
        top: 5,
        bottom: 5
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: widget.hintText
        ),
      )
    );
  }
}