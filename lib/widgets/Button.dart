import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  CustomButton({Key key, this.text, this.onPressed, this.color}) : super(key: key);

  final Color color;
  final String text;
  final Function onPressed;

  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: RaisedButton(
        color: widget.color,
        child: Text(widget.text),
        onPressed: () { widget.onPressed(); },
        elevation: 5,
      )
    );
  }
}