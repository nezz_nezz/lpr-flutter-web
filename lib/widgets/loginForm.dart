import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key key, this.signIn, this.signInLoading}) : super(key: key);

  final Function signIn;
  final bool signInLoading;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final String email='';
  final String password='';


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(width: 3, color: Theme.of(context).primaryColor),
        borderRadius: new BorderRadius.all(Radius.circular(15))
      ),
      child: Column(
        children: <Widget>[
          Text(
            "Login Form",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 20,
              fontWeight: FontWeight.w100
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: Theme.of(context).primaryColor),
              borderRadius: new BorderRadius.all(Radius.circular(5))
            ),
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(
              top: 5,
              bottom: 5
            ),
            child: TextField(
              decoration: InputDecoration(
                hintText: "Email"
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: Theme.of(context).primaryColor),
              borderRadius: new BorderRadius.all(Radius.circular(5))
            ),
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(
              top: 5,
              bottom: 5
            ),
            child: TextField(
              decoration: InputDecoration(
                hintText: "Mot de passe"
              )
            ),
          ),
          Container(
            width: 500,
            child: RaisedButton(
              color: Theme.of(context).primaryColorLight,
              child: setSignInButton(widget.signInLoading, context),
              onPressed: () {
                print("Signin In...");
                widget.signIn(email, password);
              },
              elevation: 5,
            ),
          ),
          Container(
            width: 500,
            child: RaisedButton(
            child: Text("SIGN UP"),
              elevation: 5,
              onPressed: () {
                Navigator.pushNamed(context, '/signup');
              },
              color: Color.fromRGBO(150, 150, 150, 1),
            ),
          )
        ],
      )
    );
  }
}

Widget setSignInButton(loading, context) {
  if(loading == true) {
    return new 
      Container(
        // padding: EdgeInsets.only(
        //   top: 10,
        //   bottom: 10
        // ),
        height: 20,
        width: 20,
        child: CircularProgressIndicator(
          backgroundColor: Theme.of(context).primaryColor,
        ),
     );
  } else {
    return new Text("SIGN IN");
  }
}
