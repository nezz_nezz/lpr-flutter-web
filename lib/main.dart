import 'package:flutter/material.dart';
import 'screens/loginScreen.dart';
import 'screens/signUpScreen.dart';
import 'screens/socksScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wild Socks',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.orange,
        primaryColorLight: Colors.orange.shade100
      ),
      home: MyHomePage(title: "Login",),
      routes: <String, WidgetBuilder> {
        '/signup': (BuildContext context) => SignUpScreen(title: 'Sign Up'),
        '/socks': (BuildContext context) => SocksScreen()
      },
    );
  }
}