import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:flutter/material.dart';
import 'package:lprboilerplateflutter/util/showDialog.dart';
import '../conf/secret.dart';
import '../widgets/Button.dart';
import '../widgets/textField.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key, this.title}) : super(key: key);

  final String title;

  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  final userPool = new CognitoUserPool(cognitoUserPoolId, cognitoClientId);

  test() async {
    var data;
    try {
      data = await userPool.signUp('nezz0746@gmail.com', 'Fifa@2016');
      print(data);
      Navigator.pushNamed(context, '/socks');
      openDialog(context, 'Success', 'Signed Up with Success');
    } catch (e) {
      openDialog(context, 'Sign Up Error:', e.message);
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  border: Border.all(width: 3, color: Theme.of(context).primaryColor),
                  borderRadius: new BorderRadius.all(Radius.circular(15))
                ),
                child: Column(
                  children: <Widget> [
                    Text("Sign Up"),
                    CustomTextField(hintText: "Email",),
                    CustomTextField(hintText: "Password",),
                    CustomTextField(hintText: "Confirm Password",),
                    CustomButton(
                      text: "Sign Up",
                      color: Theme.of(context).primaryColorLight,
                      onPressed: () {
                        print("Signing Up...");
                        test();
                      },
                    )
                  ]
                )
              )
            ],
          )
        ),
      ),
    );
  }
}
