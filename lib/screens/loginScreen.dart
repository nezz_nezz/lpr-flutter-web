import 'package:flutter/material.dart';
import 'package:lprboilerplateflutter/util/showDialog.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../widgets/loginForm.dart';
import '../Futures/cognito.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool signInLoading=false;

  _signIn(email, password) async {
    setState(() { signInLoading=!signInLoading; });
    print(email);
    final cognitoAuth = new CognitoAuth();
    await cognitoAuth.signIn(context, email, password);
    setState(() { signInLoading=!signInLoading; });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 350,
                margin: EdgeInsets.symmetric(
                  vertical: 20
                ),
                child: RaisedButton(
                  color: Theme.of(context).primaryColorLight,
                  onPressed: () {
                    print("Logging in with Google...");
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                        "Login with Google",
                        ),
                        padding: EdgeInsets.only(
                          left: 10
                        ),
                      )
                    ],
                  ),
                ),
              ),
              LoginForm(
                signIn: _signIn,
                signInLoading: signInLoading
              )
            ],
          )
        ),
      ),
    );
  }
}
