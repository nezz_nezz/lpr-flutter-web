import 'package:flutter/material.dart';

openDialog(context, title, text) {
      showDialog(
        context: context, 
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Signup Error:"),
            content: Text(text),
            actions: <Widget>[
              FlatButton(
                child: Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
}